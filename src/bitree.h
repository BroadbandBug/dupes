/**************************************************************************************************/
/*
 * File: bitree.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementation of a simple binary tree
 *
 */
/**************************************************************************************************/

#include "global.h"
#include <ctype.h>
/**************************************************************************************************/


#ifndef BITREE_H
#define BITREE_H


typedef struct BiTreeNode_ {
    char *word;
    int word_count;
    int balance_factor;
    struct BiTreeNode_ *right;
    struct BiTreeNode_ *left;
    struct BiTreeNode_ *parent;
    
} BiTreeNode;

typedef struct BiTree_ {
    BiTreeNode *head;
    int size;
    bool balanced;
} BiTree;

//*********************/
//HIGHER LEVEL FUNCTIONS

//Initalizes a binary search tree
void bitree_init(BiTree *tree);

//Destroys the Binary search tree specified by tree.
void bitree_destroy(BiTree *tree);

//Removes the element in the specified binary tree
bool bitree_remove(BiTree *tree, char *word);


//********************/
//LOWER LEVEL FUNCTIONS

//Insert a node to the left pointer of the specified BiTreeNode of tree
bool bitree_ins_left(BiTree *tree, BiTreeNode *node, char *word);

//Insert a node to the right pointer of the specified BiTreeNode of tree
bool bitree_ins_right(BiTree *tree, BiTreeNode *node, char *word);

//Remove the node in the left pointer of the specified BiTreeNode of tree
bool bitree_rem_left(BiTree *tree, BiTreeNode *node);

//Remove the node in the right pointer of the specified BiTreeNode of tree
bool bitree_rem_right(BiTree *tree, BiTreeNode *node);

//Return the size of the tree
int bistree_size( BiTree *tree);

//Return the position of the left node of the given node
BiTreeNode* bitree_left(BiTreeNode *node);

//Return the position of the right node of the given node
BiTreeNode* bitree_right(BiTreeNode *node);


/**************************************************************************************************/

#endif

/**************************************************************************************************/

