/**************************************************************************************************/
/*
 * File: main.c
 * Author: Kevin Klug
 * Date: 27 October 2011
 *
 * Description: source file for main() function implementation of ECE 275 
 *              Assignment 3 program dupes.
 *
 */
/**************************************************************************************************/

#include <stdio.h>
#include "bistree.h"
#include "bitree.h"
#include "dupes.h"
#include "global.h"

int main( int argc, char *argv[]){
    
    /* 
     * check for the correct number of commandline arguments. If incorrect
     * provide a simple usage message to the assit the user 
     */
	if( argc != 3 ) {
		printf("\nUsage: dupes inputfile outputfile \n\n");
        return -1;
	}
    
    //Create a binary tree
    BisTree DasTree;
    
    int maxDepth = 0;
    int minDepth = 0;
    int currentDepth = 0;
    int numLeaf = 0;
    int totalDepth = 0;
    
    //Initialize Binary Tree
    bitree_init(&DasTree);
    
    //Populate Binary tree with given file
    populate_bistree(argv[1], &DasTree);
    
    //Print the alphabetized list
    FILE *outputFile;
    outputFile = fopen(argv[2], "a+");
    bitree_print_inorder(outputFile, DasTree.head);
    
    //Find the Binary Tree Statistics
    dupes_print_stats(DasTree.head, &maxDepth, &minDepth, &currentDepth, &numLeaf, &totalDepth);
    
    //Print in Binary Tree Statistics in output file
    fprintf(outputFile, "Number of Words within Tree: %d\n", DasTree.size);
    fprintf(outputFile, "Minimum Depth of Tree: %d\n", minDepth);
    fprintf(outputFile, "Maxiumum Depth of Tree: %d\n", maxDepth);
    if (numLeaf != 0) {
        fprintf(outputFile, "Average Depth of Tree: %d\n", (totalDepth/numLeaf));
    }else{
        fprintf(outputFile, "Average Depth of Tree: 0\n");
    }
    
    fclose(outputFile);
    //Destroy the Binary Tree
    bitree_destroy(&DasTree);
    
    
	return 0;
}
