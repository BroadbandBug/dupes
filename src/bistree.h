/*
 * File: bistree.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementation of an AVL Binary Search Tree
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "bitree.h"
/**************************************************************************************************/


#ifndef BISTREE_H
#define BISTREE_H


#define AVL_LFT_HEAVY 1
#define AVL_BALANCED 0
#define AVL_RGT_HEAVY -1

typedef BiTree BisTree;

//****************************
//Binary Search Tree funcitons
//****************************

//Initalize a Binary Search Tree
void bistree_init(BisTree *tree);

//Destroy a Binary Search Tree
void bistree_destroy(BisTree *tree);

//Inserts a node in the specified binary tree
bool bistree_insert(BiTree *tree, char *word);

//Function that finds the proper place to insert a node in a balanced AVL tree
bool insert(BisTree *tree, BiTreeNode *node, char *word, int *balanced);

//Removes a node in the specified binary tree
bool bistree_remove(BisTree *tree);

//Returns a pointer to the location of the node that stores given word
BiTreeNode * bistree_lookup(BisTree *tree, char *word);

//Returns the size of the binary search tree
int bistree_size(BisTree *tree);

//*****************************************************************************************/
//The following are for low level tree rotations to ensure that the tree is sorted using AVL
//*****************************************************************************************/

//This function will either preform a LL rotation
void rotate_ll(BiTreeNode *node, BiTree *tree);

//This function will either preform a LR rotation
void rotate_lr(BiTreeNode *node, BiTree *tree);

//This function will either preform a RR rotation
void rotate_rr(BiTreeNode *node, BiTree *tree);

//This function will either preform a RL rotation
void rotate_rl(BiTreeNode *node, BiTree *tree);

#endif
