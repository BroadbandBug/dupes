/**************************************************************************************************/
/*
 * File: dupes.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assigment 3: Dupes
 *
 */
/**************************************************************************************************/
#include "global.h"
#include "bistree.h"
#include "bitree.h"
#include <stdio.h>
/**************************************************************************************************/

//Populates a given Binary Search tree with words given in the inputFileName
bool populate_bistree( char *inputFileName, BisTree* tree);

//Prints the specified binary search tree in order to an outputfile
void bitree_print_inorder(FILE *outputFile, BiTreeNode* node);

//Prints the tree Min Depth, Max Depth, Average Depth
bool dupes_print_stats( BiTreeNode *currentNode, int *maxDepth, int *minDepth, int *currentDepth, int *numLeaf, int *totalDepth);

//This function is recursivley used in bitree_print_inorder
void dupes_print_node( FILE* output, BiTreeNode *node);