/**************************************************************************************************/
/*
 * File: bistree.c
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementation of an AVL Binary Search Tree
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "bistree.h"
#include <stdlib.h>
#include <string.h>
/**************************************************************************************************/

//**********************
//HIGHER LEVEL FUNCTIONS

//Initalize a Binary Search Tree
void bistree_init(BisTree *tree){
    bitree_init(tree);
}

//Destroy a Binary Search Tree
void bistree_destroy(BisTree *tree){
    bitree_destroy(tree);
}

//Inserts a node in the specified binary tree
bool bistree_insert(BiTree *tree, char *word){
    bool balanced = 0;
    return insert(tree, tree->head, word, &balanced);
}

//Function that finds the proper place to insert a node in a balanced AVL tree
bool insert(BisTree *tree, BiTreeNode *node, char *word, int *balanced){
    
    int compare = 0;
    
    if (node == NULL) {
        return bitree_ins_left(tree, NULL, word);
    }
    
    //Use string compare to place the words in the tree alphabetically.
    //Strcmp will return a - when the first found letter is smaller than the current node's word and + when greater
    compare = strcmp(word, node->word);
    
    //The same word has been found. Increment wordcount
    if (compare == 0) {
        node->word_count++;
    }else if (compare < 0) {
        
        //Simply insert to the left pointer of node if the left most node is null
        if (node->left == NULL) {
            if (bitree_ins_left(tree, node, word)==FALSE) {
                return FALSE;
            }
            //Tree may not be balanced anymore.
            *balanced = 0;
            
        //Else, Keep recursivley calling insert to find the appropriate entry point
        }else if (insert(tree, node->left, word, balanced) != TRUE){
            return FALSE;
        }
        
//        //If the tree is not balanced after insertion
//        //This code does not work :(
//        if (*balanced != 1) {
//            
//            //Preform the appropriate ll, lr rotation to achieve a balanced tree after insertion
//            if (node->balance_factor == 1) {
//                if (node->left->balance_factor == 1) {
//                    
//                    rotate_ll(node, tree);
//                    
//                }else{
//                
//                    rotate_lr(node, tree);
//                
//                }
//                //Tree is now balanced
//                *balanced=1;
//            }
//            else if(node->balance_factor == 0){
//                node->balance_factor = 1;
//            }
//            else if(node->balance_factor ==-1){
//                node->balance_factor = 0;
//                *balanced = 1;
//            }
//        }
        
    }
    //The word to be inserted is alphabetically larger than the current node
    else if (compare  > 0){
        if (node->right == NULL) {
            if (bitree_ins_right(tree, node, word)==FALSE) {
                return FALSE;
            }
            *balanced = 0;
        }
        //Keep trying to find the correct insertion point by recursivly calling the current function
        else{
            if (insert(tree, node->right, word, balanced)!=TRUE) {
                return FALSE;
            }
        }
        
        //If the tree is not balanced after insertion
        //This code does not work :(
//        if (*balanced != 1) {
//            
//            //If insertion to right would result in balance factor of -2
//            if (node->balance_factor == -1) {
//                
//                //If insertion to the right would result in an unbalanced tree
//                if (node->right->balance_factor == -1) {
//                    rotate_rr(node, tree);
//                }else{
//                    rotate_rl(node, tree);
//                }
//                *balanced = 1;
//            }
//
//            else if(node->balance_factor == 0){
//                node->balance_factor = -1;
//            }
//            //Insertion to the right results in balance factor of 0
//            else if(node->balance_factor == 1){
//                node->balance_factor = 0;
//                *balanced = 1;
//            }
//            
//        }
                
    }
    
    return TRUE;
}

//*****************************************************************************************/
//The following are for low level tree rotations to ensure that the tree is sorted using AVL
//*****************************************************************************************/

//This function will either preform a LL rotation or a LR rotation based off of which side is heavy
void rotate_ll(BiTreeNode *node, BiTree *tree){
    
    //Declare Temp Pointer
    BiTreeNode *left;
    
    //Preform the rotation
    left = node->left;
    node->left = left->right;
    left->right = node;
    
    //Update Parents
    left->parent = node->parent;    
    node->parent = left;

    
    //Update Balance Factors
    node->balance_factor = 0;
    left->balance_factor = 0;
    
    //Update the head of the tree after rotation
    if (tree->head == node) {
        tree->head = left;
    }
    
}

//This function will preform a LR rotation
void rotate_lr(BiTreeNode *node, BiTree *tree){
    
    //Declare Temp Pointers
    BiTreeNode *left;
    BiTreeNode *grandchild;
    
    
    //Preform the rotation
    left = node->left;
    grandchild = left->right;
    left->right=grandchild->left;
    grandchild->left = left;
    node->left=grandchild->right;
    grandchild->right=node;
    
    //Update Parents
    grandchild->parent = node->parent;
    node->parent=grandchild;
    
    //Update Balance Factors
    if (grandchild->balance_factor == 1) {
        node->balance_factor = -1;
        left->balance_factor = 0;
    }else if(grandchild->balance_factor == 0){
        node->balance_factor=0;
        left->balance_factor=0;
    }else{
        node->balance_factor=0;
        left->balance_factor=1;
    }
    grandchild->balance_factor = 0;
    
    //Move the head of the tree
    if (tree->head == node) {
        tree->head = grandchild;
    }
}

//This function will either preform a RR rotation
void rotate_rr(BiTreeNode *node, BiTree *tree){
    BiTreeNode *right;
    
    //Preform the rotation
    right = node->right;
    node->right =right->left;
    right->left = node;
    
    //Update Parents
    right->parent = node->parent;
    node->parent = right;
    
    //Update Balance Factors
    node->balance_factor = 0;
    right->balance_factor = 0;
    
    //Move the head of the tree
    if (tree->head==node) {
        tree->head = node->right;
    }
    
}

//This function will either preform a RL rotation
void rotate_rl(BiTreeNode *node, BiTree *tree){
    BiTreeNode *right;
    BiTreeNode *grandchild;
    
    //Preform the rotation
    right = node->right;
    grandchild = right->left;
    right->left = grandchild->right;
    grandchild->right=right;
    node->right=grandchild->left;
    grandchild->left=node;
    
    //Update Parents
    grandchild->parent = node->parent;
    node->parent=grandchild;
    
    //Update Balance Factors for each of the three possible cases
    if (grandchild->balance_factor==1) {
        node->balance_factor=0;
        right->balance_factor=-1;
    }else if( grandchild->balance_factor==0){
        node->balance_factor=0;
        right->balance_factor=0;
    }else{
        node->balance_factor=1;
        right->balance_factor=0;
    }
    grandchild->balance_factor = 0;
    
    //Move the head of the tree
    if(tree->head == node){
        tree->head = grandchild;
    }
    
}


//Removes a node in the specified binary tree
bool bistree_remove(BisTree *tree);

//Returns a pointer to the location of the node that stores given word
BiTreeNode * bistree_lookup(BisTree *tree, char *word);

//Returns the size of the binary search tree
int bistree_size(BisTree *tree);




