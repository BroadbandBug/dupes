/**************************************************************************************************/
/*
 * File: dupes.c
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assignment 3: Dupes
 *
 */
/**************************************************************************************************/
#include "dupes.h"
#include "global.h"
#include "bitree.h"
#include "bistree.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
/**************************************************************************************************/

//***************************************************/
//Populates the Binary tree specified by inputFileName
//***************************************************/

bool populate_bistree( char *inputFileName, BisTree* tree){
    
    FILE *inputFile;
    
    char buff[BIGGESTWORD];
    char pars[BIGGESTWORD];
    int count = 0;
    //Open File
    inputFile = fopen(inputFileName, "r");
    
    //Check if file exists
    if(inputFile == NULL){
        printf("Error: Cannot open file.\n");
        return FALSE;
    }
    
    //Scan in file string by string
    while((fscanf(inputFile, "%s", buff) != 0) && !feof(inputFile)){
        
        //Remove commas, periods, and convert word to lowercase
        for (count = 0; (buff[count] != ',')&&(buff[count] != '.')&&(count<BIGGESTWORD); count++) {
            pars[count] = tolower(buff[count]);
        }
        bistree_insert(tree, pars);
        //Clear the parsed and blank_strings
        for(count = 0; count < BIGGESTWORD; count++){
            pars[count] = '\0';
        } 
    }
    fclose(inputFile);
    return TRUE;
}

//**************************************************/
//Prints the tree Min Depth, Max Depth, Average Depth
//**************************************************/
bool dupes_print_stats( BiTreeNode *currentNode, int *maxDepth, int *minDepth, int *currentDepth, int *numLeaf, int *totalDepth){

    if (currentNode == NULL) {
        *currentDepth = *currentDepth -1;
        return TRUE;
    }
    
    //If the left or the right are not Null then there are more steps go
    if ((currentNode->right != NULL)||(currentNode->left != NULL)) {
        
        if (currentNode->left !=NULL) {
            //RECURSE!
            *currentDepth = *currentDepth + 1;
            dupes_print_stats(currentNode->left, maxDepth, minDepth, currentDepth, numLeaf, totalDepth);
        }
        if (currentNode->right != NULL) {
            //RECURSE!
            *currentDepth = *currentDepth + 1;
            dupes_print_stats(currentNode->right, maxDepth, minDepth, currentDepth, numLeaf, totalDepth);
        }
        
    }else{
        //New Max depth found
        if (*currentDepth > *maxDepth) {
            *maxDepth = *currentDepth;
        }
        //Initial Case
        else if ((*currentDepth > 0)&&(*minDepth == 0)) {
            *minDepth = *currentDepth;
        }
        else if ((*currentDepth<*minDepth)){
            *minDepth = *currentDepth;
        }
        
        //Add all of the depths together
        *totalDepth = *totalDepth+*currentDepth;
        
        *numLeaf = *numLeaf + 1;
    }
    
    //Recurse BACKWARDS!
    *currentDepth = *currentDepth - 1;
    
    return TRUE;
}

//********************************************************************/
//Prints the specified binary search tree in order to the fileoutput
//********************************************************************/

void bitree_print_inorder(FILE *outputFile, BiTreeNode* node){
    if (node!=NULL) {
        bitree_print_inorder(outputFile, node->left);
        if (node->word_count>1) {
            fprintf(outputFile, "%s (%d)\n", node->word, node->word_count);
        }
        bitree_print_inorder(outputFile, node->right);    
    }
}
